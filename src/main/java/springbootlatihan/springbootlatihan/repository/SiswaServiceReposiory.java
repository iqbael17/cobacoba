/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package springbootlatihan.springbootlatihan.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import springbootlatihan.springbootlatihan.entities.Siswa;

/**
 *
 * @author Comp1996
 */
@Repository
public interface SiswaServiceReposiory extends CrudRepository<Siswa, Integer> {

    @Query(value = "select * from siswa where id = ?1", nativeQuery = true)
    Siswa getBySiswa(Integer id);

    @Query(value = "delete from siswa where id = ?1", nativeQuery = true)
    void deletebyId(Integer id);
  
     @Query(value = "  SELECT max(id)+1	FROM Siswa", nativeQuery = true)
    Integer getMaxid();
}
