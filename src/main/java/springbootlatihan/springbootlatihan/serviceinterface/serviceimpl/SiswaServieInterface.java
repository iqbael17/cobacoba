/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package springbootlatihan.springbootlatihan.serviceinterface.serviceimpl;

import java.util.List;
import springbootlatihan.springbootlatihan.entities.Siswa;

/**
 *
 * @author Comp1996
 */
public interface SiswaServieInterface {
    
    Iterable<Siswa> getAllSiswa();
    Siswa getBySiswa(Integer id);
    void DeleteSiswa(Integer id);
    Siswa savesiswa(Siswa siswa);
       Integer getMaxid();
}
