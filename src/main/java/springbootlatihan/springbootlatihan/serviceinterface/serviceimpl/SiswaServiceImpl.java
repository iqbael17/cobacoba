/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package springbootlatihan.springbootlatihan.serviceinterface.serviceimpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springbootlatihan.springbootlatihan.entities.Siswa;
import springbootlatihan.springbootlatihan.repository.SiswaServiceReposiory;

/**
 *
 * @author Comp1996
 */

@Service
public class SiswaServiceImpl  implements SiswaServieInterface{
    @Autowired
    private SiswaServiceReposiory reposiory;

    @Override
    public Iterable<Siswa> getAllSiswa() {
       return reposiory.findAll();
    }

    @Override
    public Siswa getBySiswa(Integer id) {
        return reposiory.getBySiswa(id);
    }

    @Override
    public void DeleteSiswa(Integer id) {
      reposiory.deletebyId(id);
    }

    @Override
    public Siswa savesiswa(Siswa siswa) {
    return reposiory.save(siswa);
    }

    @Override
    public Integer getMaxid() {
        return reposiory.getMaxid();
    }
    
    
}
