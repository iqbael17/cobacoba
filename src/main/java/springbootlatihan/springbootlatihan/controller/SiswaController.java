/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package springbootlatihan.springbootlatihan.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springbootlatihan.springbootlatihan.entities.Siswa;
import springbootlatihan.springbootlatihan.serviceinterface.serviceimpl.SiswaServiceImpl;
import springbootlatihan.springbootlatihan.serviceinterface.serviceimpl.SiswaServieInterface;

/**
 *
 * @author Comp1996
 */
@RestController
@RequestMapping("/log")
public class SiswaController {

    @Autowired
    private SiswaServieInterface siswaServieInterface;

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<?> getbyid(@PathVariable("id") String id) {

        return ResponseEntity.ok(siswaServieInterface.getBySiswa(Integer.parseInt(id)));

    }

    @GetMapping("/")
    @ResponseBody
    public ResponseEntity<?> getall() {

        return ResponseEntity.ok(siswaServieInterface.getAllSiswa());

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Map<String, Object> delete(@PathVariable("id") String id) {
        Map<String, Object> result = new HashMap<String, Object>();

        siswaServieInterface.DeleteSiswa(Integer.parseInt(id));

        result.put("status", "sukses");

        return result;

    }
  @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> save(@RequestBody Siswa entity) {
        Map<String, Object> result = new HashMap<String, Object>();
        String msgString = null;
        boolean status = false;
        try {

            entity.setId(siswaServieInterface.getMaxid());
            siswaServieInterface.savesiswa(entity);
            status = true;
            msgString = "Save Berhasil";


        } catch (Exception e) {

            status = false;
            msgString = "Save Error";
        }
        result.put("status", status);
        result.put("message", msgString);
        return result;

    }
    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @ResponseBody
    public Map<String, Object> update(@RequestBody Siswa entity) {
        Map<String, Object> result = new HashMap<String, Object>();
        String msgString = null;
        boolean status = false;
        try {

            Siswa s = siswaServieInterface.getBySiswa(entity.getId());
            s.setDescription(entity.getDescription());
            siswaServieInterface.savesiswa(s);
            status = true;
            msgString = "update Berhasil";


        } catch (Exception e) {

            status = false;
            msgString = "update Error Data tidak ditemukan";
        }
        result.put("status", status);
        result.put("message", msgString);
        return result;

    }
}
